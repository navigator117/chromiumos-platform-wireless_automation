# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""
Utilities that don't have a better home.
run_shell_command : Runs a shell command with a timeout.
"""

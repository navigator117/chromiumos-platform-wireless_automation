:Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
:Use of this source code is governed by a BSD-style license that can be
:found in the LICENSE file.

=====================
Wireless Automation
=====================

***************
Goal
***************

A reusable library to automate wireless testing:

* Test instrument control
    * Call box emulators
    * DC power meters
    * RF switches

* DUT control:
    * Chromebooks
    * Modems
    * Screen brightness
    * Networking stack control


************************
Style Guide
************************

pep8.py and pylint define the style. If these tools pass the code,
then the code passes.



